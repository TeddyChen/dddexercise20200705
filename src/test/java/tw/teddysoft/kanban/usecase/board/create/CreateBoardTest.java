package tw.teddysoft.kanban.usecase.board.create;

import com.google.common.eventbus.EventBus;
import org.junit.Test;
import tw.teddysoft.kanban.entity.DomainEventBus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateBoardTest {

    @Test
    public void create_board_success(){

        DomainEventBus eventBus = new DomainEventBus();
        FakeEventHandler handler = new FakeEventHandler(eventBus);
        eventBus.register(handler);

        BoardRepository boardRepository = new BoardRepository();

        CreateBoardUseCase createBoardUseCase =
                new CreateBoardUseCase(boardRepository, eventBus);

        CreateBoardInput input = new CreateBoardInput();
        CreateBoardOutput output = new CreateBoardOutput();

        input.setProjectId("001-1234");
        input.setBoardName("Scrum Board");

        createBoardUseCase.execute(input, output);


        assertNotNull(output.getBoardId());
        assertEquals("001-1234",
                boardRepository.findById(output.getBoardId()).getProjecId());

        assertEquals("Scrum Board", handler.boardCreated.getSourceName());

    }


}
