package tw.teddysoft.kanban.usecase.board.create;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import tw.teddysoft.kanban.entity.board.event.BoardCreated;

public class FakeEventHandler {

    public BoardCreated boardCreated;

//    private Repository<Board> boardRepository;
    private EventBus eventBus;

    public FakeEventHandler(EventBus eventBus){
//        this.boardRepository = boardRepository;
        this.eventBus = eventBus;
    }

    public Class<BoardCreated> subscribedToEventType() {
        return BoardCreated.class;
    }

    @Subscribe
    public void handleEvent(BoardCreated domainEvent) {

        boardCreated = domainEvent;
    }


}
