package tw.teddysoft.kanban.usecase.stage.create;

import org.junit.Before;
import org.junit.Test;
import tw.teddysoft.kanban.entity.DomainEventBus;
import tw.teddysoft.kanban.usecase.board.create.*;

import static org.junit.Assert.assertEquals;

public class CreateStageTest {

    private BoardRepository boardRepository;
    private DomainEventBus eventBus;
    private FakeEventHandler handler;
    private String boardId;

    @Before
    public void setup() {

        eventBus = new DomainEventBus();
        handler = new FakeEventHandler(eventBus);
        boardRepository = new BoardRepository();

        CreateBoardUseCase createBoardUseCase =
                new CreateBoardUseCase(boardRepository, eventBus);

        CreateBoardInput input = new CreateBoardInput();
        CreateBoardOutput output = new CreateBoardOutput();

        input.setProjectId("001-1234");
        input.setBoardName("Scrum Board");

        createBoardUseCase.execute(input, output);
        boardId = output.getBoardId();
    }

    @Test
    public void create_top_level_stage_success(){

        eventBus.register(handler);

        CreateStageUseCase createStageUseCase =
                new CreateStageUseCase(boardRepository, eventBus);

        CreateStageInput input = new CreateStageInput();
        CreateStageOutput output = new CreateStageOutput();

        input.setBoardId(boardId);
        input.setStageName("Backlog");

        createStageUseCase.execute(input, output);

        assertEquals("Backlog", handler.stageCreated.getSourceName());

    }


}
