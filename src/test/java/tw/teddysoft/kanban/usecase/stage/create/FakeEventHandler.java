package tw.teddysoft.kanban.usecase.stage.create;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import tw.teddysoft.kanban.entity.board.event.BoardCreated;
import tw.teddysoft.kanban.entity.board.event.StageCreated;

public class FakeEventHandler {

    public StageCreated stageCreated;

    private EventBus eventBus;

    public FakeEventHandler(EventBus eventBus){
        this.eventBus = eventBus;
    }

    public Class<StageCreated> subscribedToEventType() {
        return StageCreated.class;
    }

    @Subscribe
    public void handleEvent(StageCreated domainEvent) {

        stageCreated = domainEvent;
    }


}
