package tw.teddysoft.kanban.usecase.stage.create;

import tw.teddysoft.kanban.entity.DomainEventBus;
import tw.teddysoft.kanban.entity.board.Board;
import tw.teddysoft.kanban.usecase.board.create.BoardRepository;
import tw.teddysoft.kanban.usecase.board.create.CreateBoardInput;
import tw.teddysoft.kanban.usecase.board.create.CreateBoardOutput;

public class CreateStageUseCase {

    private BoardRepository boardRepository;
    private DomainEventBus eventBus;

    public CreateStageUseCase(BoardRepository boardRepository, DomainEventBus eventBus) {
        this.boardRepository = boardRepository;
        this.eventBus = eventBus;
    }

    public void execute(
            CreateStageInput input,
            CreateStageOutput output) {

        Board board = boardRepository.findById(input.getBoardId());

        board.createStage(input.getBoardId(), input.getStageName());

        boardRepository.save(board);
        eventBus.postAll(board);

    }
}
