package tw.teddysoft.kanban.usecase.board.create;

public class CreateBoardOutput {

    private String id;
    public String getBoardId() {
        return id;
    }

    public void setBoardId(String id) {
        this.id = id;
    }
}
