package tw.teddysoft.kanban.usecase.board.create;

import com.google.common.eventbus.EventBus;
import tw.teddysoft.kanban.entity.DomainEventBus;
import tw.teddysoft.kanban.entity.board.Board;

public class CreateBoardUseCase {
    private BoardRepository boardRepository;
    private DomainEventBus eventBus;

    public CreateBoardUseCase(BoardRepository boardRepository, DomainEventBus eventBus) {
        this.boardRepository = boardRepository;
        this.eventBus = eventBus;
    }

    public void execute(
            CreateBoardInput input,
            CreateBoardOutput output) {

    Board board = new Board(input.getProjectId(), input.getBoardName());



    boardRepository.save(board);

    eventBus.postAll(board);

    output.setBoardId(board.getId());

    }
}
