package tw.teddysoft.kanban.usecase.board.create;

import tw.teddysoft.kanban.entity.board.Board;

import java.util.ArrayList;
import java.util.List;

public class BoardRepository {
    private List<Board> boards;

    public BoardRepository(){
        boards = new ArrayList<Board>();
    }

    public Board findById(String boardId) {
        for(Board board :boards){
            if(board.getId().equalsIgnoreCase(boardId))
                return board;
        }
        throw new RuntimeException("Board not found");
    }

    public void save(Board board) {
        boards.add(board);
    }
}
