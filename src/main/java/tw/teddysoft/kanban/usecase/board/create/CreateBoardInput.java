package tw.teddysoft.kanban.usecase.board.create;

public class CreateBoardInput {
    private String projectId;
    private String boardName;

    public void setProjectId(String s) {
        projectId = s;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getBoardName() {
        return boardName;
    }
}
