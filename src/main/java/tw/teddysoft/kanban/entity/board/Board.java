package tw.teddysoft.kanban.entity.board;

import tw.teddysoft.kanban.entity.AggregateRoot;
import tw.teddysoft.kanban.entity.board.event.BoardCreated;
import tw.teddysoft.kanban.entity.board.event.StageCreated;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Board extends AggregateRoot {

    private String projectId;

    private List<Stage> stages;

    public Board(String projectId, String boardName){
        super(boardName);
        this.projectId = projectId;
        stages = new ArrayList<>();

        addDomainEvent(new BoardCreated(this.getId(), boardName, projectId));


    }

    public String getId() {
        return id;
    }

    public String getProjecId() {
        return projectId;
    }

    public void createStage(String boardId, String stageName) {
        Stage stage = new Stage(boardId, stageName);
        stages.add(stage);
        this.addDomainEvent(new StageCreated(stage.getId(), stageName, boardId));
    }


}
