package tw.teddysoft.kanban.entity.board.event;


import tw.teddysoft.kanban.entity.AbstractDomainEvent;
import tw.teddysoft.kanban.entity.Entity;
import tw.teddysoft.kanban.entity.board.Board;

public class BoardCreated extends AbstractDomainEvent {

    private String projectId;

    public BoardCreated(String id, String name, String projectId) {
        super(id, name);
        this.projectId = projectId;
    }

    public BoardCreated(Entity entity) {
        super(entity);
    }

    @Override
    public Board getEntity(){
        return (Board) super.getEntity();
    }

    public String getProjectId() {
        return projectId;
    }
}
