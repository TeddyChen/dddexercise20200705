package tw.teddysoft.kanban.entity.board;

import tw.teddysoft.kanban.entity.Entity;

public class Stage extends Entity {
    private String boardId;

    public Stage(String boardId, String name){
        super(name);
        this.boardId = boardId;
    }

}
