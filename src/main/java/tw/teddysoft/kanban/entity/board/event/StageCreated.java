package tw.teddysoft.kanban.entity.board.event;


import tw.teddysoft.kanban.entity.AbstractDomainEvent;
import tw.teddysoft.kanban.entity.Entity;
import tw.teddysoft.kanban.entity.board.Board;

public class StageCreated extends AbstractDomainEvent {

    private String boardId;

    public StageCreated(String id, String name, String boardId) {
        super(id, name);
        this.boardId = boardId;
    }

    public StageCreated(Entity entity) {
        super(entity);
    }

    @Override
    public Board getEntity(){
        return (Board) super.getEntity();
    }

    public String getBoardIdId() {
        return boardId;
    }
}
